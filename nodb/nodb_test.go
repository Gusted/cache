// Copyright 2014 The Macaron Authors
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package nodb

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitea.com/go-chi/cache"
)

func Test_LedisCacher(t *testing.T) {
	opt := cache.Options{
		Adapter:       "nodb",
		AdapterConfig: "./tmp.db",
	}

	t.Run("Basic operations", func(t *testing.T) {
		c, err := cache.NewCacher(opt)
		if !assert.NoError(t, err) || !assert.NotNil(t, c) {
			return
		}

		assert.NoError(t, c.Put("uname", "some-user-name", 1))
		assert.NoError(t, c.Put("uname2", "unknwon2", 1))
		assert.True(t, c.IsExist("uname"))

		assert.Nil(t, c.Get("404"))
		assert.EqualValues(t, "some-user-name", c.Get("uname"))

		time.Sleep(2 * time.Second)
		assert.Nil(t, c.Get("uname"))
		time.Sleep(1 * time.Second)
		assert.Nil(t, c.Get("uname2"))

		assert.NoError(t, c.Put("uname", "some-user-name", 0))
		assert.NoError(t, c.Delete("uname"))
		assert.Nil(t, c.Get("uname"))

		assert.NoError(t, c.Put("uname", "some-user-name", 0))
		assert.NoError(t, c.Flush())
		assert.Nil(t, c.Get("uname"))

		assert.Error(t, c.Incr("404"))
		assert.Error(t, c.Decr("404"))

		assert.NoError(t, c.Put("int", 0, 0))
		assert.NoError(t, c.Put("int64", int64(0), 0))
		assert.NoError(t, c.Put("string", "hi", 0))

		assert.NoError(t, c.Incr("int"))
		assert.NoError(t, c.Incr("int64"))

		assert.NoError(t, c.Decr("int"))
		assert.NoError(t, c.Decr("int64"))

		assert.Error(t, c.Incr("string"))
		assert.Error(t, c.Decr("string"))

		assert.EqualValues(t, "0", c.Get("int"))
		assert.EqualValues(t, "0", c.Get("int64"))

		assert.NoError(t, c.Flush())
	})
}
